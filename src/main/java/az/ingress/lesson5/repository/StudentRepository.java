package az.ingress.lesson5.repository;

import az.ingress.lesson5.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Integer> {
}
