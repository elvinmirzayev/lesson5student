package az.ingress.lesson5.service;

import az.ingress.lesson5.model.Student;

public interface StudentService {
    Student get(Integer id);

    Student create(Student student);

    Student update(Long id, Student student);

    Student update(Integer id, Student student);

    void delete(Integer id);
}
