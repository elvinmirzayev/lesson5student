package az.ingress.lesson5.service;

import az.ingress.lesson5.model.Student;
import az.ingress.lesson5.repository.StudentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class StudentServiceImp implements StudentService {
    private final StudentRepository studentRepository;

    public StudentServiceImp(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public Student get(Integer id) {
        log.info("Student get service started!");
        Optional<Student> student = studentRepository.findById(id);
        studentRepository.findById(id);
        if (student.isEmpty()) {
            throw new RuntimeException("Student is not found!");
        }
        return student.get();

    }

    @Override
    public Student create(Student student) {
        log.info("Student service create method is working");
        Student studentInDb = studentRepository.save(student);
        return studentInDb;
    }

    @Override
    public Student update(Long id, Student student) {
        return null;
    }

    @Override
    public Student update(Integer id, Student student) {
        log.info("Student service update method is working");
        Student entity = studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Student is not found"));
        entity.setAge(student.getAge());
        entity.setName(student.getName());
        entity.setSurname(student.getSurname());
        entity.setPhone(student.getPhone());
        entity = studentRepository.save(entity);
        return entity;
    }

    @Override
    public void delete(Integer id) {
        log.info("Student service delete method is working");
        studentRepository.deleteById(id);
    }
}

