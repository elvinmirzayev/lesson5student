package az.ingress.lesson5.factory;

public class CreatorA extends Creator{

@Override
    public Product createProduct(){
        return new ProductA();
    }
}
