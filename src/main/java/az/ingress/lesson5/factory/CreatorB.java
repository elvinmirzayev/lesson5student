package az.ingress.lesson5.factory;

public class CreatorB extends Creator{

    @Override
    public Product createProduct(){
        return new ProductB();
    }
}

